// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations

function [txt] = ptccgen_emit_interface(my_config, cpr, data)
    // Emit the interface function code
    //
    // Calling Sequence
    //   [txt] = ptccgen_emit_interface(my_config, data)
    //
    // Parameters
    // my_config: the configuration data
    // cpr: the compiled state
    // data: the compiled data
    //
    // Return
    // txt: string matrix: the text (lines of string) to export.
    //

    // Extract data
    rdnom = my_config.out.name;

    // Emit code
    
    clkinput=ones(clkIN)';
    clkoutput=ones(clkOUT)';
    //outtb=outtb;
    oz=cpr.state.oz;

    new_oz=list();
    for i=1:lstsize(oz)
        new_oz($+1) = oz(i)
    end
    for i=1:lstsize(outtb)
        new_oz($+1) = outtb(i)
    end

    //outtb($+1) = zeros(nblk,1);
    txt=['function [x,y,typ]='+rdnom+'_c(job,arg1,arg2)';
    '// Emited by PThread Codegen';
    '// at ' + date();
    ' x=[];y=[];typ=[];';
    ' select job';
    ' case ''plot'' then';
    '   standard_draw(arg1)';
    ' case ''getinputs'' then';
    '   [x,y,typ]=standard_inputs(arg1)';
    ' case ''getoutputs'' then';
    '   [x,y,typ]=standard_outputs(arg1)';
    ' case ''getorigin'' then';
    '   [x,y]=standard_origin(arg1)';
    ' case ''set'' then';
    '   x=arg1;';
    ' case ''define'' then'
    '   '+sci2exp(capt(:,3),'in',70); //input ports sizes 1
    '   '+sci2exp(capt(:,4),'in2',70); //input ports sizes 2
    '   '+sci2exp(scs_c_nb2scs_nb(capt(:,5)),'intyp',70); //input ports type
    '   '+sci2exp(actt(:,3),'out',70); //output ports sizes 1
    '   '+sci2exp(actt(:,4),'out2',70); //output ports sizes 2
    '   '+sci2exp(scs_c_nb2scs_nb(actt(:,5)),'outtyp',70); //output ports type
    '   '+sci2exp(x,'x',70); //initial continuous state
    '   '+sci2exp(z,'z',70); //initial discrete state
    '   work=zeros('+string(nblk)+',1)';
    '   Z=[z;work]';
    '   '+sci2exp(new_oz,'odstate',70);
    '   '+sci2exp(cpr.sim.rpar,'rpar',70); //real parameters
    '   '+sci2exp(cpr.sim.ipar,'ipar',70); //integer parameters
    '   '+sci2exp(cpr.sim.opar,'opar',70); //object parameters
    '   '+sci2exp(clkinput,'clkinput',70);
    '   '+sci2exp(clkoutput,'clkoutput',70);
    '   '+sci2exp(FIRING,'firing',70);
    '   nzcross='+string(sum(cpr.sim.zcptr(2:$)-cpr.sim.zcptr(1:$-1)))';
    '   nmode='+string(sum(cpr.sim.modptr(2:$)-cpr.sim.modptr(1:$-1)))';]

    for i=1:length(bllst)
        deput=[%t,%f]
        if (bllst(i).dep_ut(2) == %t) then
            deput(1,2)=%t;
            break;
        end
    end
    txt($+1)='   '+sci2exp(deput,'dep_ut',70);
    txt=[txt
    '   model=scicos_model(sim=list('''+rdnom+''',4),..'
    '                      in=in,..'
    '                      in2=in2,..'
    '                      intyp=intyp,..'
    '                      out=out,..'
    '                      out2=out2,..'
    '                      outtyp=outtyp,..'
    '                      evtin=clkinput,..'
    '                      evtout=clkoutput,..'
    '                      firing=firing,..'
    '                      state=x,..'
    '                      dstate=Z,..'
    '                      odstate=odstate,..'
    '                      rpar=rpar,..'
    '                      ipar=ipar,..'
    '                      opar=opar,..'
    '                      blocktype=''c'',..'
    '                      dep_ut=dep_ut,..'
    '                      nzcross=nzcross,..'
    '                      nmode=nmode)'
    '   gr_i=''xstringb(orig(1),orig(2),'''''+rdnom+''''',sz(1),..'
    '          sz(2),''''fill'''')''';
    '   x=standard_define([2 2],model,[],gr_i)';
    ' end'
    'endfunction'];
endfunction

//scs_c_nb2scs_nb : scicos C number to scicos number
//
//input : c_nb  : the scicos C number type
//
//output : scs_nb : the scilab number type
//
//16/06/07 Author : A.Layec
function [scs_nb]=scs_c_nb2scs_nb(c_nb)
 scs_nb=zeros(size(c_nb,1),size(c_nb,2));
 for i=1:size(c_nb,1)
   for j=1:size(c_nb,2)
     select (c_nb(i,j))
       case 10 then
         scs_nb(i,j) = 1
       case 11 then
         scs_nb(i,j) = 2
       case 81 then
         scs_nb(i,j) = 5
       case 82 then
         scs_nb(i,j) = 4
       case 84 then
         scs_nb(i,j) = 3
       case 811 then
         scs_nb(i,j) = 8
       case 812 then
         scs_nb(i,j) = 7
       case 814 then
         scs_nb(i,j) = 6
       else
         scs_nb(i,j) = 1
     end
   end
 end
endfunction
//