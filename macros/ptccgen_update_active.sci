// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations


function ALWAYS_ACTIVE = ptccgen_update_active(bllst)
    // Update the children bloc list if the superblock should be always active.
    //
    // Calling Sequence
    //   ALWAYS_ACTIVE = ptccgen_update_active(bllst)
    //
    // Parameters
    // bllst: list of blocks: compiled block list
    // ALWAYS_ACTIVE: boolean: Flag set if the superblock should be always active.
    //

    //  when any block is always active then 
    //      all the generated code should be always active.

    ALWAYS_ACTIVE=%f;
    for blki=bllst
        if blki.dep_ut($) then
            ALWAYS_ACTIVE=%t;
            break;
        end
    end
    if ALWAYS_ACTIVE then
        for Ii=1:length(bllst)
            if part(bllst(Ii).sim(1),1:7)=='capteur' then
                bllst(Ii).dep_ut($)=%t
            end
        end
    end
endfunction

