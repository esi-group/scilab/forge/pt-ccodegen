// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations

function targetDirBtn_cb()
    targetDirTxt = findobj("Tag", "ptccgen_targetDirTxt");
    
    baseDir = get(targetDirTxt, "String");
    if ~isdir(baseDir) then
        baseDir = SCIHOME;
    end
    set(targetDirTxt, "String", uigetdir(baseDir));
endfunction
