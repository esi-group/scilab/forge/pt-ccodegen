// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2013 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations

function [fds, state] = ptccgen_emit_call_sensor(fds, state, cpr, i)
    // Emit the input data call (according to the external API)
    //
    // Calling Sequence
    //   [txt, state] = ptccgen_emit_call_sensor(txt, state, cpr, i);
    //
    // Parameters
    // fds: (filename, file descriptor)'s list
    // state: the current state (data stash)
    // cpr: scicos_cpr: the current cpr
    // i: double: index of the block
    //
    // Return
    // fds: updated (filename, file descriptor)'s list
    // state: the current state (data stash)
    //
    
    // getting number of I/O
    nin = inpptr(i+1) - inpptr(i);
    nout = outptr(i+1) - outptr(i);
    
    // get input function types and kind
    if funtyp(i)==0 then
        if nout==0 then
            yk    = 0;
            nyk_1 = 0;
            nyk_2 = 0;
            yk_t  = 1;
        else
            yk    = outlnk(outptr(i));
            nyk_1 = size(outtb(yk),1);
            nyk_2 = size(outtb(yk),2);
            yk_t  = ptccgen_utils_toCosType(outtb(yk));
        end
    end
    
    // update the global sensor stash
    capt=[i yk nyk_1 nyk_2 yk_t bllst(i).ipar]
    state.capt = [state.capt ; capt]
    
    // get the bloc name
    rdnom = state.config.out.name;
    
    // get the sequencer txt
    sequencer = txt("sequence");
    
    // emit sequencer txt
    Code($+1)='block_'+rdnom+'['+string(i-1)+'].nevprt=nevprt;'
    Code=['/* Call of sensor (blk nb '+string(i)+') */'
        Code;
        'nport = '+string(nbcap)+';';
        rdnom+'_sensor(&flag, &nport, &block_'+rdnom+'['+string(i-1)+'].nevprt, '+..
        'told, ('+mat2scs_c_ptr(outtb(yk))+' *)(outtbptr['+string(yk-1)+']), &nrd_'+string(nyk_1)+..
        ', &nrd_'+string(nyk_2)+', &nrd_'+string(yk_t)+',aaa);'];
    
    // set the sequencer txt
    sequencer($+1) = Code;
    txt("sequence") = sequencer;
    clear sequencer Code;
    
    // get the declaration txt
    declaration = txt("declaration");
    
    // emit the declaration txt
    proto='void '+rdnom+'_sensor('+..
        'int* flag, int* nport, int* nevprt, double* told, void* outtbptr, int* nrd0, int* nrd1, int* nrd2, int aaa);'
   
    // set the declaration txt
    declaration($+1) = proto;
    txt("declaration") = declaration;
endfunction
