// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations


function [txt] = ptccgen_emit_file_incl (txt, data)
    // Short description on the first line following the function header.
    //
    // Calling Sequence
    //   [txt] = ptccgen_emit_file_incl (txt, data)
    //
    // Parameters
    // txt: struct(): the text (lines of string) to export.
    // data: anything: data that can be used on this macro.
    //
    
    // FIXME: not implemented yet
    error("Not implemented yet")
endfunction

