// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations


function ptccgen_generate(scs_m, out_path)
    // Generate the code for the given scs_m diagram to the path_out directory
    //
    // Calling Sequence
    //   ptccgen_generate (scs_m, out_path)
    //
    // Parameters
    // scs_m: diagram: A superblock diagram to use for code generation
    // out_path: path string: Existing directory where to export the code.
    //
    if ~exists("scicos_diagram") then loadXcosLibs(); end
    
    [lhs, rhs] = argn();
    if rhs <> 2 then
        error(msprintf("%s: Wrong number of input argument(s): %d expected.\n", "ptccgen_generate", 2))
    end
    if typeof(scs_m) <> "diagram" then
        error(msprintf("%s: Wrong type for input argument #%d: diagram expected.\n", "ptccgen_generate", 1))
    end
    if typeof(out_path) <> "string" | length(out_path) == 0 then
        error(msprintf("%s: Wrong size for input argument #%d: Non-empty string expected.\n", "ptccgen_generate", 2))
    end
    
    [IN, OUT, clkIN, clkOUT, scs_m] = ptccgen_update_io(scs_m);

    // pre-compile and check for event inputs
    szclkIN=size(clkIN,2);
    if szclkIN==0 then
        szclkIN=[]
    end
    flgcdgen=szclkIN;
    [bllst, connectmat, clkconnect, cor, corinv, ok, scs_m, flgcdgen, freof] = c_pass1(scs_m, flgcdgen);
    if flgcdgen<> szclkIN
        clkIN=[clkIN flgcdgen]
    end
    szclkINTemp=szclkIN;
    szclkIN=flgcdgen;

    if ~ok then
        error(msprintf(gettext("%s: error in the precompilation phase."), "ptccgen_generate"));
    end

    ALWAYS_ACTIVE = ptccgen_update_active(bllst);
    [act, cap, allhowclk, allhowclk2] = ptccgen_get_io(bllst);
    [bllst, clkconnect, corinv, howclk] = ptccgen_update_sb_eio(bllst, szclkIN,..
                                                ALWAYS_ACTIVE, clkconnect, corinv, allhowclk, allhowclk2, clkIN);
    FIRING = ptccgen_get_firing(bllst, clkconnect, allhowclk2);
    
    // Compile the model to a simulation structure
    Code_gene_run=[]; // flag for code generator
    cpr=c_pass2(bllst,connectmat,clkconnect,cor,corinv)    
    if cpr == list() then
        error(msprintf(gettext("%s: compilation failed.\n"), "ptccgen_generate"))
    end

    ptccgen_validate_funs(cpr.sim);
    with_work = ptccgen_get_with_work(cpr);

    // everything is fine, so start the UI
    my_config = ptccgen_config();
    my_config.out.name = ptccgen_utils_toCName(scs_m.props.title(1));
    if isdir(out_path) then
        my_config.out.workingdir = out_path;
    end
    
    data = struct(..
        "ALWAYS_ACTIVE", ALWAYS_ACTIVE,..
        "io", list(act, cap, allhowclk, allhowclk2),..
        "FIRING", FIRING,..
        "clk", list(clkIN, clkOUT),..
        "bllst", bllst);
    
    my_config = ptccgen_gui(my_config, cpr, data);
endfunction

