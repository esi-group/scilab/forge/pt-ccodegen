function [c_nb]=ptccgen_utils_toCosType(outtb)
    // Convert the c_pass1 outtb matrix descriptor to a native (eg scicos) type number
    //
    // Calling Sequence
    //   [c_nb]=ptccgen_utils_toCosType(outtb)
    //
    // Parameters
    // outtb: (typed) matrix: real or integer matrix
    //
    // Return
    // c_nb: integer: scicos_sim type descriptor number (used to pass data)

 select type(outtb)
   //real matrix
   case 1 then
      if isreal(outtb) then
        c_nb = 10
      else
        c_nb = 11
      end
   //integer matrix
   case 8 then
      select typeof(outtb)
         case 'int32' then
           c_nb = 84
         case 'int16' then
           c_nb = 82
         case 'int8' then
           c_nb = 81
         case 'uint32' then
           c_nb = 814
         case 'uint16' then
           c_nb = 812
         case 'uint8' then
           c_nb = 811
      end
   else
     break;
 end
endfunction

