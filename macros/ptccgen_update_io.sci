// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations


function [IN, OUT, clkIN, clkOUT, scs_m] = ptccgen_update_io(scs_m)
    // Flag any I/O ports with unique abstract function name and 
    // interfacefunctions.
    //
    // The abstract function names are :
    //     - 'input_n' where n is a block number for data inputs
    //     - 'output_n' where n is a block number for data outputs
    //     - 'bidon' for event inputs
    //     - 'bidon2' for event outputs
    //
    // Calling Sequence
    //   [IN, OUT, clkIN, clkOUT] = ptccgen_updateIO(scs_m);
    //
    // Parameters
    // scs_m: a diagram: The current diagram to work on
    // IN: vector: Input ports numbers
    // OUT: vector: Output ports numbers
    // clkIN: vector: Event input ports numbers
    // clkOUT: vector: Event output ports numbers

    // getting i/o port numbers
    IN=[]; OUT=[]; clkIN=[]; clkOUT=[];
    n_in=0; n_out=0;

    for i=1:size(scs_m.objs)
        if typeof(scs_m.objs(i))=='Block' then
            if scs_m.objs(i).gui=='IN_f' then
                n_in = n_in + 1;

                // remove IN_f from any inner loop with this flag
                scs_m.objs(i).gui = "INPUTPORTEVTS";
                scs_m.objs(i).model.sim(1)='actionneur'+string(n_in)

                // add a depend on t port for block activation
                scs_m.objs(i).model.evtin=1

                // add the in index to the loop
                IN=[IN scs_m.objs(i).model.ipar]
            elseif scs_m.objs(i).gui=='OUT_f' then
                n_out = n_out + 1;
                
                // remove OUT_f from any inner loop with this flag
                scs_m.objs(i).gui = "OUTPUTPORTEVTS";
                scs_m.objs(i).model.sim(1)='capteur'+string(n_out)

                // add the out index to the loop
                OUT=[OUT scs_m.objs(i).model.ipar]
            elseif scs_m.objs(i).gui=='CLKINV_f' then

                scs_m.objs(i).gui='EVTGEN_f';
                scs_m.objs(i).model.sim(1)='bidon'
                clkIN=[clkIN scs_m.objs(i).model.ipar];
            elseif scs_m.objs(i).gui=='CLKOUTV_f' then
                scs_m.objs(i).model.sim(1)='bidon2'
                clkOUT=[clkOUT scs_m.objs(i).model.ipar]
            end
        end
    end

    // Error checking
    IN=-gsort(-IN);
    if or(IN<>[1:size(IN,'*')]) then
        error(msprintf(gettext("%s: input ports are not numbered properly.", "ptccgen_update_io")));
    end
    OUT=-gsort(-OUT);
    if or(OUT<>[1:size(OUT,'*')]) then
        error(msprintf(gettext("%s: output ports are not numbered properly.", "ptccgen_update_io")));
    end
    clkIN=-gsort(-clkIN);
    if or(clkIN<>[1:size(clkIN,'*')]) then
        error(msprintf(gettext("%s: event input ports are not numbered properly.", "ptccgen_update_io")));
    end
    clkOUT=-gsort(-clkOUT);
    if or(clkOUT<>[1:size(clkOUT,'*')]) then
        error(msprintf(gettext("%s: event output ports are not numbered properly.", "ptccgen_update_io")));
    end
endfunction

