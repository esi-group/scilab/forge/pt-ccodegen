// This file is part of the Pthread C Code generator toolbox
// 
// Copyright (C) 2011 - DIGITEO - Clément DAVID <clement.david@scilab.org>
// Copyright (C) 2011-2012 - Scilab Enterprises - Clément DAVID <clement.david@scilab-enterprises.com>
// see license.txt for more licensing informations


function [txt, state] = ptccgen_emit_call_actor (txt, state, cpr, i)
    // Emit the output data call (according to the external API)
    //
    // Calling Sequence
    //   [txt, state] = ptccgen_emit_call_actor (txt, state, cpr, i)
    //
    // Parameters
    // txt: struct: used as key (file) - txt (code string matrix) mapping
    // state: the current state (data stash)
    // cpr: scicos_cpr: the current cpr
    // i: double: index of the block
    //
    // Return
    // txt: string matrix: the text (lines of string) to export.
    // state: the current state (data stash)
    //

    // getting number of I/O
    nin = inpptr(i+1) - inpptr(i);
    nout = outptr(i+1) - outptr(i);

    // get output function types and kind
    if funtyp(i)==0 then
        if nin==0 then
            uk    = 0;
            nuk_1 = 0;
            nuk_2 = 0;
            uk_t  = 1;
        else
            uk    = inplnk(inpptr(i));
            nuk_1 = size(outtb(uk),1);
            nuk_2 = size(outtb(uk),2);
            uk_t  = ptccgen_utils_toCosType(outtb(uk));
        end
    end
    
    // update the global actor stash
    actt=[i uk nuk_1 nuk_2 uk_t bllst(i).ipar]
    state.actt = [state.actt ; actt]
    
    // get the bloc name
    rdnom = state.config.out.name;
    
    // get the sequencer txt
    sequencer = txt.sequence;
    
    // emit sequencer txt
    Code='block_'+rdnom+'['+string(i-1)+'].nevprt=nevprt;'
    Code=['/* Call of actuator (blk nb '+string(i)+') */'
        Code;
        'nport = '+string(nbact)+';';
        rdnom+'_actuator(&flag, &nport, &block_'+rdnom+'['+string(i-1)+'].nevprt, told, '+..
        '('+mat2scs_c_ptr(outtb(uk))+' *)outtbptr['+string(uk-1)+'], &nrd_'+string(nuk_1)+', &nrd_'+..
        string(nuk_2)+', &nrd_'+string(uk_t)+',bbb);'];
    
    // set the sequencer txt
    sequencer($+1) = Code;
    txt.sequence = sequencer;
    clear sequencer Code;
    
    // get the declaration txt
    declaration = txt.declaration;
    
    // emit the declaration txt
    proto='void '+rdnom+'_actuator('+..
        'int* flag, int* nport, int* nevprt, double* told, void* outtbptr, int* nrd0, int* nrd1, int* nrd2, int bbb);'
   
    // set the declaration txt
    declaration($+1) = proto;
    txt.declaration = declaration;
endfunction